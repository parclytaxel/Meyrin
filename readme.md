On the ConwayLife forums there are _five different_ threads dealing with conduits:

* [Stable Herschel, excluding H-to-G](https://www.conwaylife.com/forums/viewtopic.php?f=2&t=1599)
* [Stable H-to-G](https://www.conwaylife.com/forums/viewtopic.php?f=2&t=1682)
* [Stable elementary](https://www.conwaylife.com/forums/viewtopic.php?f=2&t=1849)
* [Periodic Herschel](https://www.conwaylife.com/forums/viewtopic.php?f=2&t=4790)
* [Periodic elementary](https://www.conwaylife.com/forums/viewtopic.php?f=2&t=4996)

This repository **Meyrin**, named after [the location](https://en.wikipedia.org/wiki/Meyrin) of the Large Hadron Collider where lots of conduits push particles around, collects in one place the (complete) conduits posted in all those threads and beyond.

from meyrin import lt
oriens = ("identity", "rot90", "rot180", "rot270", "flip_y", "swap_xy_flip", "flip_x", "swap_xy")
orien_letters = "FLBRflbr"
ocodes = dict(zip(oriens, orien_letters))
inv_ocodes = dict(zip(orien_letters, oriens))
glider_direcs = {"q": "identity", "p": "flip_x", "d": "flip_y", "b": "rot180"}

def min_tf(source, target, limit=100):
    """Compute the minimal transformation (n, o, dx, dy), where the order for o is FLBRflbr,
    such that source[n](o, dx, dy) == target."""
    tw = target.wechsler
    for n in range(limit):
        if source[n].wechsler == tw:
            sn = source[n]
            tx, ty = target.getrect()[:2]
            for o in oriens:
                sx, sy = sn(o).getrect()[:2]
                dx = tx - sx
                dy = ty - sy
                if sn(o, dx, dy) == target:
                    return (n, o, dx, dy)

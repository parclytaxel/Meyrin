from meyrin import lt
from .transforms import ocodes, inv_ocodes, glider_direcs, min_tf

particles = {"B": lt.pattern("o$bo$b2o$2o$o")(-1,-2),
             "C": lt.pattern("b3o$3bo$ob2o$2o")(-2,-1),
             "D": lt.pattern("2b2o$bobo$o2bo$obo$bo")(-2,-1),
             "H": lt.pattern("o$obo$3o$2bo")(-1,-2),
             "H3": lt.pattern("b2o$3ob2o$b2ob3o$3ob2o$2o")(-5,-2),
             "H5": lt.pattern("bo$b3o$b3o$o3bo$4o$ob2o")(-4,-3),
             "H6": lt.pattern("bo$2b2o$3b2o$5bo$5o$3bo")(-5,-3),
             "I": lt.pattern("3b2o$bo2bo$o3bo$bobo$2bo")(-3,-1),
             "J": lt.pattern("o$o$o$b2o$b2o")(-1,-2),
             "O": lt.pattern("b2o$2b2o$3o$o")(-2,-1),
             "P": lt.pattern("3o$2bo$3o")(-1,-1),
             "Q": lt.pattern("o$2o$b2o$b3o$b2o$2o$o")(-2,-3),
             "R": lt.pattern("bo$b2o$2o")(-1,-1),
             "S": lt.pattern("2bo$b2o$2o$o")(-1,-1),
             "U": lt.pattern("2o$3o$2obo$2b2o")(-2,-2),
             "W": lt.pattern("b2o$2bo$2o$o")(-1,-1),
             "G": lt.pattern("bo$2bo$3o")(-1,-1),
             "Lw": lt.pattern("o2bo$4bo$o3bo$b4o")(-4,-2),
             "Mw": lt.pattern("2bo$o3bo$5bo$o4bo$b5o")(-5,-3),
             "Hw": lt.pattern("2b2o$o4bo$6bo$o5bo$b6o")(-6,-3)}

def decode_condproper(cond_str):
    """Given a string representing the conduit itself (a stationary periodic object),
    return a Pattern corresponding to that string."""
    tmp, _, transform = cond_str.partition("@")
    apgcode, _, phase = tmp.partition("+")
    pat = lt.pattern(apgcode)[int(phase) if phase else 0]
    o = inv_ocodes[transform[0]]
    dx, dy = map(int, transform[1:].split(","))
    return pat(o, dx, dy)

def decode_outglider(outg_str):
    """Given a string representing a glider output, return a glider matched in time and space."""
    t, _, lane = outg_str.partition(":")
    direc, lane_n = glider_direcs[lane[0]], int(lane[1:])
    return particles["G"](direc, 0, lane_n)[-int(t)]

def decode_output(out_str):
    """Given a string representing a conduit output that is not a glider,
    return a pair (appearance time, transformed Pattern) representing that output."""
    t, _, tmp = out_str.partition(":")
    obj, _, transform = tmp.partition("@")
    pat = particles[obj]
    o = inv_ocodes[transform[0]] # this may change when symmetric objects are considered
    dx, dy = map(int, transform[1:].split(","))
    return (int(t), pat(o, dx, dy))

def decode_cond(condline):
    """Decode a conduit line, returning a pair whose first element is a Pattern with
    the input object in the conduit and whose second object is a list of outputs."""
    partin, _, tmp = condline.partition(" ")
    condproper, _, partout = tmp.partition(" ")
    outputs = []
    if partin == "H":
        outputs.append(decode_outglider("21:p-2"))
    elif partin == "H3":
        outputs.append(decode_outglider("111:p10")) # count generations from the provided input
    for output in partout.split():
        if output == "ng" and partin in ("H", "H3"):
            outputs.pop(0)
        elif "@" not in output:
            outputs.append(decode_outglider(output))
        else:
            outputs.append(decode_output(output))
    assembly = particles[partin] + decode_condproper(condproper)
    return (assembly, outputs)

def encode_cond(condrle):
    """Encode a conduit for storage in Meyrin; assumes the active object is in
    its canonical phase and orientation."""
    assembly = lt.pattern(condrle)
    for (k, particle) in particles.items():
        if (m := assembly.match(particle, halo="3o$3o$3o")):
            dx, dy = m.coords()[0]
            assembly = assembly(-dx, -dy)
            condproper = assembly - particle
            cpapg = condproper.apgcode
            n, o, dx, dy = min_tf(lt.pattern(cpapg), condproper, condproper.period)
            phase = "" if n == 0 else f"+{n}"
            astr = f"{k} {cpapg}{phase}@{ocodes[o]}{dx},{dy}"
            return astr
